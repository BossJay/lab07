public enum Square
{
  X,
  O,
  BLANK {
    @Override
    public String toString() {
      return "_";
    }
  }
}