import java.util.Scanner;

public class TicTacToeGame
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    boolean continueGame = true;
    int player1Wins = 0;
    int player2Wins = 0;
    
    while(continueGame == true)
    { 
      System.out.println("Welcome to a game of Tic-Tac-Toe!");
      
      Board boardGame = new Board();
      boolean gameOver = false;
      int player = 1;
      Square playerToken = Square.X;
    
      while (gameOver == false)
      {
        System.out.println(boardGame);
    
        if (player == 1)
        {
          playerToken = Square.X;
        }
        else
        {
          playerToken = Square.O;
        }
        
        System.out.println("Player " + player + ":");
        System.out.println("What Row");
        int row = scan.nextInt();
        System.out.println("What Column");
        int col = scan.nextInt();
        
        boolean valid = boardGame.placeToken(row, col, playerToken);
        
        while (valid == false)
        {
          System.out.println("Invalid data! Please re-enter inputs.");
          
          System.out.println("Player " + player + ":");
          System.out.println("What Row");
          row = scan.nextInt();
          System.out.println("What Column");
          col = scan.nextInt();
          
          valid = boardGame.placeToken(row, col, playerToken);
        }
        
        if (boardGame.checkIfFull() == true)
        {
          System.out.println(boardGame);
          System.out.println("It's a tie!");
          
          gameOver = true;
        }
        else if (boardGame.checkIfWinning(playerToken) == true)
        {
          System.out.println(boardGame);
          
          if (player == 1)
          {
            System.out.println("Player 1 is the winner!");
            
            player1Wins++; 
          }
          else
          {
            System.out.println("Player 2 is the winner!");
            
            player2Wins++;
          }
          gameOver = true;
        }
        else
        {
          player += 1;
          
          if (player > 2)
          {
            player = 1;
          }
        }
      }
      
      System.out.println("Would you like to play another game? Type '0' for YES or '1' for NO");
      int continueInput = scan.nextInt();
      while(continueInput > 1)
      {
        System.out.println("Invalid Input! Would you like to play another game? Type '0' for YES or '1' for NO");
        continueInput = scan.nextInt(); 
      }
      
      if (continueInput == 1)
      {
        System.out.println("Thanks for playing!");
        System.out.println("Player 1 wins: " + player1Wins + "\nPlayer 2 wins: " + player2Wins);
        continueGame = false;
      }
    }
  }
}