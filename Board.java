public class Board
{
  private Square[][] tictactoeBoard;
  
  public Board()
  {
    tictactoeBoard = new Square[3][3];
    
    for (int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        tictactoeBoard[i][j] = Square.BLANK;
      }
    }
  }
  
  public String toString()
  {
    String stringToReturn = "  0 1 2\n";
    
    for (int i = 0; i < tictactoeBoard.length; i++)
    {
      stringToReturn += i + " ";
      
      for(int j = 0; j < tictactoeBoard.length; j++)
      {
        stringToReturn += (tictactoeBoard[i][j] + " ");
      }
      
      stringToReturn += "\n";
    }
    
    return stringToReturn;
  }
  
  public boolean placeToken (int row, int col, Square playerToken)
  {
    boolean validation = true;
    
    if (row > 2 || col > 2)
    {
      validation = false;
    }
    else if (tictactoeBoard[row][col] == Square.BLANK)
    {
      tictactoeBoard[row][col] = playerToken;
      validation = true;
    }
    else
    {
      validation = false;
    }
    
    return validation;
  }
  
  public boolean checkIfFull()
  {
    boolean validation = true;
    
    for (int i = 0; i < tictactoeBoard.length; i++)
    {
      for(int j = 0; j < tictactoeBoard.length; j++)
      {
        if(tictactoeBoard[i][j] == Square.BLANK)
        {
          validation = false;
        }
      }
    }
    
    return validation;
  }
  
  private boolean checkIfWinningHorizontal(Square playerToken)
  {
    boolean validation = false;
    
    for (int i = 0; i < tictactoeBoard.length; i++)
    {
      if ((tictactoeBoard[i][0] == playerToken) && (tictactoeBoard[i][1] == playerToken) && (tictactoeBoard[i][2] == playerToken))
      {
        validation = true;
      }
    }
    
    return validation;
  }
  
  private boolean checkIfWinningVertical(Square playerToken)
  {
    boolean validation = false;
    
    for (int i = 0; i < tictactoeBoard.length; i++)
    {
      if ((tictactoeBoard[0][i] == playerToken) && (tictactoeBoard[1][i] == playerToken) && (tictactoeBoard[2][i] == playerToken))
      {
        validation = true;
      }
    }
    
    return validation;
  }
  
  public boolean checkIfWinning(Square playerToken)
  {
    boolean validation = false;
    boolean resultVertical = checkIfWinningVertical(playerToken);
    boolean resultHorizontal = checkIfWinningHorizontal(playerToken);
    boolean resultDiagonal = checkIfWinningDiagonal(playerToken);
    
    if (resultVertical == true || resultHorizontal == true || resultDiagonal == true)
    {
      validation = true;
    }
    
    return validation;
  }
  
  private boolean checkIfWinningDiagonal(Square playerToken)
  {
    boolean validation = false;
    
    if(tictactoeBoard[0][0] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][2] == playerToken)
    {
      validation = true;
    }
      
    if(tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][0] == playerToken)
    {
      validation = true;
    }  
    
    return validation;
  }
}